/*
 * Copyright (c) 2022, AlAndrew
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

extern char** static_argv;
extern unsigned int static_argc;